# Author: Olivier Lenoir
# Created: 2022-06-14 17:44:32


FROM alpine:latest

ARG USER
ARG USER_NAME

RUN \
    # Upgrade OS
    apk -U upgrade && \
    # Install vim, make, git
    apk add --no-cache \
        vim \
        make \
        git && \
    # Add user ${USER}
    adduser -D -g '${USER_NAME}' ${USER}

USER ${USER}

WORKDIR /home/${USER}

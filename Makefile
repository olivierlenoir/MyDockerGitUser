# Author: Olivier Lenoir - <olivier.len02@gmail.com>
# Created: 2022-06-14 17:42:20
# Project: MyDockerGitUser
# Description: Dockerfile image and container based on git config


FILE = filename
TODAY = $(shell date +'%Y%m%d')
USER_EMAIL = $(shell git config --get user.email)
USER_NAME = $(shell git config --get user.name)
IMAGE = $(shell echo "my_alpine_${USER}:latest")
CONTAINER = $(shell echo "MyAlpine-${USER}")


all: ;


.PHONY: all build create start attach stop remove destroy variables show root deploy save load


.SILENT: all build create start attach stop remove destroy variables show root deploy save load


variables:
	echo "FILE: ${FILE}"
	echo "TODAY: ${TODAY}"
	echo "USER: ${USER}"
	echo "USER_EMAIL: ${USER_EMAIL}"
	echo "USER_NAME: ${USER_NAME}"
	echo "IMAGE: ${IMAGE}"
	echo "CONTAINER: ${CONTAINER}"
#	echo ": ${}"


build: destroy
	echo "Build docker image: ${IMAGE}"
	docker build --no-cache \
	--build-arg USER_NAME='${USER_NAME}' \
	--build-arg USER=${USER} \
	-t ${IMAGE} .


create: remove
	echo "Create docker container: ${CONTAINER}"
	# Create shared volume
	mkdir -p /home/${USER}/MyDockerData
	# Create container
	docker create -it \
		-v /home/${USER}/MyDockerData:/mnt/MyDockerData \
		--name ${CONTAINER} ${IMAGE} /bin/sh
	make start
	# Git config default branch, user name and user email
	docker exec -it ${CONTAINER} \
		sh -c "git config --global init.defaultBranch master && \
			git config --global user.email ${USER_EMAIL} && \
			git config --global user.name '${USER_NAME}' && \
			ln -s /mnt/MyDockerData"
	make stop


start:
	docker start ${CONTAINER}


attach: start
	docker attach ${CONTAINER}


stop:
	docker stop ${CONTAINER} || true


remove: stop
	docker container rm -f ${CONTAINER}


destroy: remove
	echo "Remove docker image: ${IMAGE}"
	docker image rm -f ${IMAGE}


root: start
	docker exec -it --user root ${CONTAINER} /bin/sh


show:
	echo "Images:"
	docker images
	echo "Containers:"
	docker container ls -a


deploy: build create attach


save:
	mkdir -p archive
	docker save --output archive/${IMAGE}.tar ${IMAGE}

load: destroy
	docker load --input archive/${IMAGE}.tar
	make create
